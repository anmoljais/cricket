Server Requirements:
- Apache
- MySQL
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

Installing Composer:
- php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
- HASH="$(wget -q -O - https://composer.github.io/installer.sig)"
- php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
- sudo php composer-setup.php --install-dir=/usr/local/bin --filename=composer

Step to configure:
- Get the code from git.
- Goto project directory execute command: composer install
- Copy .env_example as .env
- Configure APP configuration in .env file.
- Generate APP key:
	php artisan key:generate
- Setup the database
	- Configure database DB_HOST/DB_PORT/DB_DATABASE/DB_USERNAME/DB_PASSWORD in .env file
	- Create database 'cricket'
	- Perform table migration to create table structures. Execute below command:
		- php artisan migrate
	- Dump dummy data in tables. Execute below command:
		- php artisan db:seed --class=TeamsSeeder
		- php artisan db:seed --class=PlayersSeeder
		- php artisan db:seed --class=MatchesSeeder
- Configure Document root in apache conf file to the public directory.


