<?php

use Illuminate\Database\Seeder;

class MatchesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('matches')->insert([
        	['team_1' => 1,'team_1_score' => 148,'team_1_wicket' => 7,'team_2' => 5,'team_2_score' => 149,'team_2_wicket' => 8],
        	['team_1' => 6,'team_1_score' => 162,'team_1_wicket' => 8,'team_2' => 1,'team_2_score' => 161,'team_2_wicket' => 7],
        	['team_1' => 2,'team_1_score' => 165,'team_1_wicket' => 8,'team_2' => 7,'team_2_score' => 162,'team_2_wicket' => 8],
        	['team_1' => 3,'team_1_score' => 170,'team_1_wicket' => 5,'team_2' => 1,'team_2_score' => 173,'team_2_wicket' => 3],
        	['team_1' => 2,'team_1_score' => 125,'team_1_wicket' => 9,'team_2' => 6,'team_2_score' => 125,'team_2_wicket' => 2],
        	['team_1' => 1,'team_1_score' => 176,'team_1_wicket' => 4,'team_2' => 7,'team_2_score' => 175,'team_2_wicket' => 3],
        	['team_1' => 3,'team_1_score' => 213,'team_1_wicket' => 4,'team_2' => 5,'team_2_score' => 211,'team_2_wicket' => 6],
        	['team_1' => 1,'team_1_score' => 162,'team_1_wicket' => 5,'team_2' => 4,'team_2_score' => 162,'team_2_wicket' => 8],
        	['team_1' => 5,'team_1_score' => 198,'team_1_wicket' => 7,'team_2' => 7,'team_2_score' => 167,'team_2_wicket' => 8],
        	['team_1' => 1,'team_1_score' => 151,'team_1_wicket' => 4,'team_2' => 2,'team_2_score' => 147,'team_2_wicket' => 9],
        ]);
    }
}
