<?php

use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
        	['id' => 1,'name' => 'Chennai Super King','logo_uri' => 'images/teams_logo/Chennai-Super-Kings-Logo-PNG.png','club_state' => 'Chennai'],
        	['id' => 2,'name' => 'Delhi Daredevils','logo_uri' => 'images/teams_logo/Delhi-Daredevils-Logo-PNG.png','club_state' => 'Delhi'],
        	['id' => 3,'name' => 'King Xl Punjab','logo_uri' => 'images/teams_logo/Kings-XI-Punjab-Logo-PNG.png','club_state' => 'Punjab'],
        	['id' => 4,'name' => 'Kolkata Knight Rider','logo_uri' => 'images/teams_logo/Kolkata-Knight-Riders-Logo-PNG.png','club_state' => 'Kolkata'],
        	['id' => 5,'name' => 'Mumbai Indian','logo_uri' => 'images/teams_logo/Mumbai-Indians-Logo-PNG.png','club_state' => 'Mumbai'],
        	['id' => 6,'name' => 'Royal Challenger Banglore','logo_uri' => 'images/teams_logo/Royal-Challengers-Bangalore-Logo-PNG.png','club_state' => 'Banglore'],
        	['id' => 7,'name' => 'Sunrise Hyderabad','logo_uri' => 'images/teams_logo/sunrise_hyderabad.png','club_state' => 'Hyderabad']
        ]);
    }
}
