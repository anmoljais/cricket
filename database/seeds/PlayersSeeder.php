<?php

use Illuminate\Database\Seeder;

class PlayersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('players')->insert([
        	['id' => 1,'first_name' => 'MS','last_name' => 'Dhoni','image_uri' => 'images/player_image/ms-dhoni.png','jersey_number' => 7,'country' => 'India','team_id' => 1],
        	['id' => 2,'first_name' => 'Suresh','last_name' => 'Raina','image_uri' => 'images/player_image/suresh-raina.png','jersey_number' => 48,'country' => 'India','team_id' => 1],
        	['id' => 3,'first_name' => 'Deepak','last_name' => 'Chahar','image_uri' => 'images/player_image/Deepak_Chahar.png','jersey_number' => 90,'country' => 'India','team_id' => 1],
        	['id' => 4,'first_name' => 'Karn','last_name' => 'Sharma','image_uri' => 'images/player_image/Karn-Sharma.png','jersey_number' => 36,'country' => 'India','team_id' => 1],
        	['id' => 5,'first_name' => 'Ravindra','last_name' => 'Jadeja','image_uri' => 'images/player_image/Ravindra-Jadeja.png','jersey_number' => 8,'country' => 'India','team_id' => 1],
        	['id' => 6,'first_name' => 'Rishabh','last_name' => 'Pant','image_uri' => 'images/player_image/Rishabh-Pant.png','jersey_number' => 17,'country' => 'India','team_id' => 2],
        	['id' => 7,'first_name' => 'Amit','last_name' => 'Mishra','image_uri' => 'images/player_image/Amit-Mishra.png','jersey_number' => 99,'country' => 'India','team_id' => 2],
        	['id' => 8,'first_name' => 'Avesh','last_name' => 'Khan','image_uri' => 'images/player_image/Avesh-Khan.png','jersey_number' => 6,'country' => 'India','team_id' => 2],
        	['id' => 9,'first_name' => 'Harshal','last_name' => 'Patel','image_uri' => 'images/player_image/Harshal-Patel.png','jersey_number' => 13,'country' => 'India','team_id' => 2],
        	['id' => 10,'first_name' => 'Chris','last_name' => 'Gayle','image_uri' => 'images/player_image/Chris-Gayle.png','jersey_number' => 333,'country' => 'West Indies','team_id' => 3],
        	['id' => 11,'first_name' => 'K. L.','last_name' => 'Rahul','image_uri' => 'images/player_image/KL-Rahul.png','jersey_number' => 1,'country' => 'India','team_id' => 3],	
        	['id' => 12,'first_name' => 'Dinesh','last_name' => 'Karthik','image_uri' => 'images/player_image/Dinesh-Karthik.png','jersey_number' => 19,'country' => 'India','team_id' => 4],
        	['id' => 13,'first_name' => 'Andre','last_name' => 'Russell','image_uri' => 'images/player_image/Andre-Russell.png','jersey_number' => 12,'country' => 'India','team_id' => 4],
        	['id' => 14,'first_name' => 'Kuldeep','last_name' => 'Yadav','image_uri' => 'images/player_image/kuldeep-yadav.png','jersey_number' => 23,'country' => 'India','team_id' => 4],
        	['id' => 15,'first_name' => 'Rohit','last_name' => 'Sharma','image_uri' => 'images/player_image/Rohit-Sharma.png','jersey_number' => 45,'country' => 'India','team_id' => 5],
        	['id' => 16,'first_name' => 'Hardik','last_name' => 'Pandya','image_uri' => 'images/player_image/Hardik-Pandya.png','jersey_number' => 33,'country' => 'India','team_id' => 5],
        	['id' => 17,'first_name' => 'Lasith','last_name' => 'Malinga','image_uri' => 'images/player_image/Lasith-Malinga.png','jersey_number' => 99,'country' => 'Sri Lanka','team_id' => 5],
        	['id' => 18,'first_name' => 'Virat','last_name' => 'Kohli','image_uri' => 'images/player_image/Virat-Kohli.png','jersey_number' => 18,'country' => 'India','team_id' => 6],
        	['id' => 19,'first_name' => 'AB de','last_name' => 'Villiers','image_uri' => 'images/player_image/AB-de-Villiers.png','jersey_number' => 17,'country' => 'South Africa	','team_id' => 6],
        	['id' => 20,'first_name' => 'Moeen','last_name' => 'Ali','image_uri' => 'images/player_image/Moeen-Ali.png','jersey_number' => 8,'country' => 'England','team_id' => 6],
        	['id' => 21,'first_name' => 'Gurkeerat','last_name' => 'Singh','image_uri' => 'images/player_image/Gurkeerat-Singh.png','jersey_number' => 29,'country' => 'India','team_id' => 6],
        	['id' => 22,'first_name' => 'David','last_name' => 'Warner','image_uri' => 'images/player_image/David-Warner.png','jersey_number' => 31,'country' => 'Australia','team_id' => 7],
        	['id' => 23,'first_name' => 'Manish','last_name' => 'Pandey','image_uri' => 'images/player_image/Manish-Pandey.png','jersey_number' => 21,'country' => 'India	','team_id' => 7],
        	['id' => 24,'first_name' => 'Kane','last_name' => 'Williamson','image_uri' => 'images/player_image/Kane-Williamson.png','jersey_number' => 22,'country' => 'Australia','team_id' => 7]
        	]);
    }
}
