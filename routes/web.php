<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@teamsList')->name('Teams');
Route::get('/listPlayers/{id}', 'HomeController@teamPlayersList')->name('Players');
Route::get('/listPlayers', 'HomeController@teamPlayersList')->name('Players');
Route::get('/listMatches', 'HomeController@matchesList')->name('Matches');