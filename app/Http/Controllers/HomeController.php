<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Teams;
use App\model\Players;
use App\model\Matches;

class HomeController extends Controller
{
    public function teamsList()
    {
        $teams = Teams::all();
        return view('teams')->with(compact('teams'));
    }

    public function teamPlayersList($teamId=NULL)
    {
        if (!empty($teamId))
        {
            $players = Players::where('team_id', '=', $teamId)->get();
            $teams = Teams::find($teamId);
            $teamName = $teams->name;
            return view('players')->with(compact('players','teamName'));
        }
        else
        {
            $players = Players::all();
            return view('players')->with(compact('players'));
        }
    }

    public function matchesList()
    {
        $matches = Matches::all();
        foreach($matches as $match)
        {
            if($match->team_1_score > $match->team_2_score)
                $match['winner_team'] = $match['team_1'];
            else if($match->team_1_score < $match->team_2_score)
                $match->winner_team = $match->team_2;
            else
                $match->winner_team = 0;
        }

        $teamsDetail = Teams::all()->keyBy('id');
        return view('matches')->with(compact('matches','teamsDetail'));
    }
}
