@extends('layout.mainlayout')
@section('content')

	<div class="container-fluid" style="margin-top:80px">
		<h4 style="width: 60%; margin-left:20%; text-align: center; font-size: xx-large;">
			{{isset($teamName) ? $teamName."'s" : ""}}
			Players
		</h4>

		<div class="row" style="width: 80%; margin-left:10%">
			@foreach($players as $player)
				<div class="card col-sm-12 col-md-5 col-lg-5 col-xl-5 shadow" style="margin:2%">
					<img class="card-img-top" src="{{ asset($player->image_uri) }}" style="width:100%; height: 400px">
					<div class="card-body">
						<h4 class="card-title">{{$player->first_name." ".$player->last_name}}</h4>
						<p class="card-text">Jersey: {{$player->jersey_number}}</p>
						<p class="card-text">Country: {{$player->country}}</p>
					</div>
				</div>
			@endforeach
		</div>
	</div>

@endsection
