@extends('layout.mainlayout')
@section('content')

		<div class="container-fluid" style="margin-top:80px">
			<div class="widget-content nopadding" style="width: 80%; margin-left:10%">
				<table class="table table-striped table-hover">
					<tbody>
						<tr class="row table-secondary">
							<td colspan="3" style="width: 100%; text-align: center; font-size: xx-large;">Teams</td>
						</tr>

						@foreach($teams as $team)
						<tr class="row shadow">
							<td class="col-sm-1 col-md-1 col-lg-1 col-xl-1">
								<img src="{{ asset($team->logo_uri) }}" style="height: 50px; width: 50px">
							</td>
							<td class="col-sm-7 col-md-7 col-lg-7 col-xl-7">
								{{$team->name}}
							</td>
							<td class="col-sm-4 col-md-4 col-lg-4 col-xl-4">
								<button type="button" class="btn btn-primary float-right" onclick="location.href='/listPlayers/{{$team->id}}'">View Details</button>
							</td>
						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>

@endsection
