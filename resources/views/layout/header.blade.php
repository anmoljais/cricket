<nav class="navbar navbar-expand-sm bg-dark navbar-dark fixed-top">
	<ul class="navbar-nav">
		<li class="nav-item">
			<a class="nav-link" href="/">Teams</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/listPlayers">Players</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/listMatches">Matches</a>
		</li>
	</ul>
</nav>
