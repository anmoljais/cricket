@extends('layout.mainlayout')
@section('content')

		<div class="container-fluid" style="margin-top:80px">
			<div class="widget-content nopadding" style="width: 80%; margin-left:10%">
				<table class="table table-striped table-hover">
					<tbody>
						<tr class="row table-secondary">
							<td colspan="3" style="width: 100%; text-align: center; font-size: xx-large;">Matches</td>
						</tr>

						@foreach($matches as $match)
						<tr class="row shadow">
							@php
								if ($match['winner_team'] == 0)
								{
									$team1Class = "btn-secondary";
									$team2Class = "btn-secondary";	
								}
								else if ($match['team_1'] == $match['winner_team'])
								{
									$team1Class = "btn-success";
									$team2Class = "btn-danger";
								}
								else
								{
									$team1Class = "btn-danger";
									$team2Class = "btn-success";
								}
							@endphp 	


							<td class="col-sm-5 col-md-5 col-lg-5 col-xl-5 float-right">
								<img class="float-right" src="{{ asset($teamsDetail[$match['team_1']]['logo_uri']) }}" style="height: 50px; width: 50px; margin-right: 10px; margin-left: 10px">
								<button type="button" class="btn {{$team1Class}} float-right">
									{{$teamsDetail[$match['team_1']]['name']}} 
									<span class="badge badge-light">{{$match['team_1_score']}} / {{$match['team_1_wicket']}}</span>
								</button>
							</td>

							<td class="col-sm-2 col-md-2 col-lg-2 col-xl-2" style="text-align: center; font-size: xx-large;">
								V
							</td>

							<td class="col-sm-5 col-md-5 col-lg-5 col-xl-5 float-left">
								<img class="float-left" src="{{asset($teamsDetail[$match['team_2']]['logo_uri'])}}" style="height: 50px; width: 50px; margin-right: 10px; margin-left: 10px">
								<button type="button" class="btn {{$team2Class}} float-left">
									{{$teamsDetail[$match['team_2']]['name']}} 
									<span class="badge badge-light">{{$match['team_2_score']}} / {{$match['team_2_wicket']}}</span>
								</button>
							</td>

						</tr>
						@endforeach

					</tbody>
				</table>
			</div>
		</div>

@endsection
